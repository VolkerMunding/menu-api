# Node.js and TypeScript Tutorial: Build a CRUD API

<https://auth0.com/blog/node-js-and-typescript-tutorial-build-a-crud-api/>

Learn how to use TypeScript to build a feature-complete Express API. Tutorial on how to use TypeScript with Express to create, read, update, and delete data.

This tutorial shows you how to build a feature-complete API using Node.js, Express, and TypeScript that lets clients perform data operations on resources that describe a restaurant menu. Using TypeScript with Node.js gives you access to optional static type-checking along with robust tooling for large apps and the latest ECMAScript features.

Learn also how to define data models, create a data service, and quickly build modular endpoints. As an option, you can also learn how to secure the API using Auth0.
